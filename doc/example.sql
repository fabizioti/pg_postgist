DROP DATABASE IF EXISTS pg_postgist;

CREATE DATABASE  pg_postgist;

\c  pg_postgist

CREATE EXTENSION postgist CASCADE;

SELECT spatiotemporal_make('ST_TRAJECTORY(2015-05-18 10:00:00;2015-05-18 11:00:00;POINT(2 4)2015-05-18 10:00:00;POINT(5 6)2015-05-18 10:00:00;)');  


SELECT spatiotemporal_make('ST_TRAJECTORY(2015-05-18 10:00:00;2015-05-20 15:00:00;POINT(2 4)2015-05-18 10:00:00;POINT(3 4)2015-05-18 11:00:00;POINT(4 6)2015-05-19 13:00:00;)');

SELECT st_get_location_y(spatiotemporal_make('ST_TRAJECTORY(2015-05-18 10:00:00;2015-05-20 15:00:00;POINT(2 4)2015-05-18 10:00:00;POINT(3 4)2015-05-18 11:00:00;POINT(4 6)2015-05-19 13:00:00;)'), 1);

SELECT st_get_location_x(spatiotemporal_make('ST_TRAJECTORY(2015-05-18 10:00:00;2015-05-20 15:00:00;POINT(2 4)2015-05-18 10:00:00;POINT(3 4)2015-05-18 11:00:00;POINT(4 6)2015-05-19 13:00:00;)'), 1);

SELECT st_get_time(spatiotemporal_make('ST_TRAJECTORY(2015-05-18 10:00:00;2015-05-20 15:00:00;POINT(2 4)2015-05-18 10:00:00;POINT(3 4)2015-05-18 11:00:00;POINT(4 6)2015-05-19 13:00:00;)'), 1);

SELECT get_duration(spatiotemporal_make('ST_TRAJECTORY(2015-05-18 10:00:00;2015-05-20 15:00:00;POINT(2 4)2015-05-18 10:00:00;POINT(3 4)2015-05-18 11:00:00;POINT(4 6)2015-05-19 13:00:00;)'));
