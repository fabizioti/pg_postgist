/*
  Copyright (C) 2017 National Institute For Space Research (INPE) - Brazil.

  postgis-t is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 3 as
  published by the Free Software Foundation.

  postgis-t is distributed  "AS-IS" in the hope that it will be useful,
  but WITHOUT ANY WARRANTY OF ANY KIND; without even the implied warranty
  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with postgis-t. See LICENSE. If not, write to
  Gilberto Ribeiro de Queiroz at <gribeiro@dpi.inpe.br>.

 */

/*!
 *
 * \file postgist/spatiotemporal.c
 *
 * \brief Spatial-Temporal Geographic Objects
 *
 * \author Gilberto Ribeiro de Queiroz
 * \author Fabiana Zioti
 *
 * \date 2017
 *
 * \copyright GNU Lesser Public License version 3
 *
 */

/* PostGIS-T extension */
#include "spatiotemporal.h"
#include "wkt.h"
#include "hexutils.h"

/* PostgreSQL */
#include <libpq/pqformat.h>
#include <utils/builtins.h>
#include <utils/rangetypes.h>
#include <utils/array.h>
#include <catalog/pg_type.h>
#include <utils/lsyscache.h>



PG_FUNCTION_INFO_V1(spatiotemporal_make);

Datum
spatiotemporal_make(PG_FUNCTION_ARGS)
{

  char *str = PG_GETARG_CSTRING(0);

  struct spatiotemporal *st = NULL;

  /*get number of TRAJECTORY element*/
  int number_coords = coord_count(str) - 2;


  int base_size = number_coords * sizeof(struct coord2d);

  int size = offsetof(struct spatiotemporal, coords) + base_size;

  st = (struct spatiotemporal*) palloc(size);

  SET_VARSIZE(st, size);

  st->dummy = 0;

  st->npts = number_coords;

  st->srid = 0;

  spatiotemporal_decode(str, st);


  PG_RETURN_SPATIOTEMPORAL_P(st);
}


PG_FUNCTION_INFO_V1(spatiotemporal_in);

Datum
spatiotemporal_in(PG_FUNCTION_ARGS)
{

  char *str = PG_GETARG_CSTRING(0);

  char *hstr = str;

  struct spatiotemporal *st = NULL;

  int hstr_size = strlen(str);

  int size = offsetof(struct spatiotemporal, npts) + (hstr_size / 2);

  st = (struct spatiotemporal*) palloc(size);

  SET_VARSIZE(st, size);
  st->dummy = 0;

  /* decode the hex-string */
  hex2binary(hstr, hstr_size, (char*)(&st->npts));

  PG_RETURN_SPATIOTEMPORAL_P(st);

}

PG_FUNCTION_INFO_V1(spatiotemporal_out);

Datum
spatiotemporal_out(PG_FUNCTION_ARGS)
{

  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  int size = ( 2 * sizeof(int32) + 2 * sizeof(Timestamp)) + (st->npts * sizeof(struct coord2d));

  /* alloc a buffer for hex-string plus a trailing '\0' */
  char *hstr = palloc(2 * size + 1);

  /*elog(NOTICE, "spatiotemporal called");*/

  if (!PointerIsValid(st))
    ereport(ERROR, (errcode (ERRCODE_INVALID_PARAMETER_VALUE),
                    errmsg("missing argument for spatiotemporal")));

  binary2hex((char*)(&st->npts), size, hstr);

  PG_RETURN_CSTRING(hstr);

}



PG_FUNCTION_INFO_V1(spatiotemporal_as_text);

Datum
spatiotemporal_as_text(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  char *start_time;

  char *end_time;

  StringInfoData str;

  initStringInfo(&str);

  start_time = DatumGetCString(DirectFunctionCall1(timestamp_out, st->start_time));
  end_time = DatumGetCString(DirectFunctionCall1(timestamp_out, st->end_time));

  appendStringInfoString(&str, start_time);

  appendStringInfoChar(&str, ',');

  appendStringInfoString(&str, end_time);

  pfree(start_time);
  pfree(end_time);

  PG_RETURN_CSTRING(str.data);

}


PG_FUNCTION_INFO_V1(spatiotemporal_duration);

Datum
spatiotemporal_duration(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  Interval *result = DatumGetIntervalP(DirectFunctionCall2(timestamp_mi, st->end_time, st->start_time));

  PG_RETURN_INTERVAL_P(result);

}


PG_FUNCTION_INFO_V1(spatiotemporal_get_start_time);

Datum
spatiotemporal_get_start_time(PG_FUNCTION_ARGS)
{
 struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

 PG_RETURN_TIMESTAMP(st->start_time);
}

PG_FUNCTION_INFO_V1(spatiotemporal_get_end_time);

Datum
spatiotemporal_get_end_time(PG_FUNCTION_ARGS)
{
 struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

 PG_RETURN_TIMESTAMP(st->end_time);
}


PG_FUNCTION_INFO_V1(spatiotemporal_get_location_x);

Datum
spatiotemporal_get_location_x(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  int position = PG_GETARG_INT32(1);

  PG_RETURN_FLOAT8(st->coords[position].x);
}

PG_FUNCTION_INFO_V1(spatiotemporal_get_location_y);

Datum
spatiotemporal_get_location_y(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  int position = PG_GETARG_INT32(1);

  PG_RETURN_FLOAT8(st->coords[position].y);

}

PG_FUNCTION_INFO_V1(spatiotemporal_get_time);

Datum
spatiotemporal_get_time(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  int position = PG_GETARG_INT32(1);

  PG_RETURN_TIMESTAMP(st->coords[position].time);

}

PG_FUNCTION_INFO_V1(spatiotemporal_array_location_x);

Datum
spatiotemporal_array_location_x(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  ArrayType  *result_array;

  Datum *datum_elems;

  int16 typlen;

  bool typbyval;

  char typalign;

  datum_elems = (Datum *) palloc((st->npts) * sizeof(Datum));

  for (int i = 0; i < st->npts; i++)
  {
    datum_elems[i] = Float8GetDatum(st->coords[i].x);
  }

  get_typlenbyvalalign(FLOAT8OID, &typlen, &typbyval, &typalign);

  /* construct 1-D array*/
  result_array = construct_array(datum_elems, st->npts, FLOAT8OID, typlen, typbyval, typalign);

  PG_RETURN_ARRAYTYPE_P(result_array);
}


PG_FUNCTION_INFO_V1(spatiotemporal_array_location_y);

Datum
spatiotemporal_array_location_y(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  ArrayType  *result_array;

  Datum *datum_elems;

  int16 typlen;

  bool typbyval;

  char typalign;

  datum_elems = (Datum *) palloc((st->npts) * sizeof(Datum));

  for (int i = 0; i < st->npts; i++)
  {
    datum_elems[i] = Float8GetDatum(st->coords[i].y);
  }

  get_typlenbyvalalign(FLOAT8OID, &typlen, &typbyval, &typalign);

  /* construct 1-D array*/
  result_array = construct_array(datum_elems, st->npts, FLOAT8OID, typlen, typbyval, typalign);

  PG_RETURN_ARRAYTYPE_P(result_array);
}


PG_FUNCTION_INFO_V1(spatiotemporal_array_get_time);

Datum
spatiotemporal_array_get_time(PG_FUNCTION_ARGS)
{
  struct spatiotemporal *st = PG_GETARG_SPATIOTEMPORAL_P(0);

  ArrayType  *result_array;

  Datum *datum_elems;

  int16 typlen;

  bool typbyval;

  char typalign;

  datum_elems = (Datum *) palloc((st->npts) * sizeof(Datum));

  for (int i = 0; i < st->npts; i++)
  {
    datum_elems[i] = TimestampGetDatum(st->coords[i].time);
  }

  get_typlenbyvalalign(TIMESTAMPOID, &typlen, &typbyval, &typalign);

  /* construct 1-D array*/
  result_array = construct_array(datum_elems, st->npts, TIMESTAMPOID, typlen, typbyval, typalign);

  PG_RETURN_ARRAYTYPE_P(result_array);
}
