/*
  Copyright (C) 2017 National Institute For Space Research (INPE) - Brazil.

  postgis-t is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 3 as
  published by the Free Software Foundation.

  postgis-t is distributed  "AS-IS" in the hope that it will be useful,
  but WITHOUT ANY WARRANTY OF ANY KIND; without even the implied warranty
  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with postgis-t. See LICENSE. If not, write to
  Gilberto Ribeiro de Queiroz at <gribeiro@dpi.inpe.br>.

 */

/*!
 *
 * \file postgist/wkt.C
 *
 * \brief Conversion routine between Well-Kown Text respresentation and geometric objects.
 *
 * \author Gilberto Ribeiro de Queiroz
 * \author Fabiana Zioti
 *
 * \date 2017
 *
 * \copyright GNU Lesser Public License version 3
 *
 */

/*PostGIS-T extension*/
#include "wkt.h"

/* PostgreSQL */
#include <libpq/pqformat.h>
#include <utils/builtins.h>


/* C Standard Library */
#include <assert.h>
#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <string.h>

/*
 * WKT delimiters for input/output
 */
#define ST_WKT_TOKEN "ST_"
#define ST_WKT_TOKEN_LEN 3

#define TRAJECTORY_WKT_TOKEN "TRAJECTORY"
#define TRAJECTORY_WKT_TOKEN_LEN 10

#define GEOEXT_GEOPOINT_WKT_TOKEN "POINT"
#define GEOEXT_GEOPOINT_WKT_TOKEN_LEN 5

#define LDELIM '('
#define RDELIM ')'
#define COLLECTION_DELIM ';'




int coord_count(char *s)
{
  int ndelim = 0;

  while ((s = strchr(s, COLLECTION_DELIM)) != NULL)
  {
    ++ndelim;
    ++s;
  }

  return ndelim;
}


inline
Timestamp timestamp_decode(char **cp)
{
	char *time;

	char *t;

	int index;

	t = strchr(*cp, ';');

	index = (int)(t - (*cp));

	time = (char*) palloc(index + 1);

	memset(time, '\0', index + 1);

	strncpy(time, *cp, index);

	/*memcpy(time, *cp, index);*/

	//time[index + 1] = '\0';

	*cp += index;

	return DatumGetTimestamp(DirectFunctionCall3(timestamp_in, PointerGetDatum(time), PointerGetDatum(1114), PointerGetDatum(-1)));

}

static inline void
coord2d_decode(char *str,
               struct coord2d *coord,
               char **endptr,
               const char *type_name,
               const char *orig_string)
{
  assert(str);
  assert(coord);
  assert(endptr);
  assert(type_name);
  assert(orig_string);

  coord->x = float8in_internal(str, &str, type_name, orig_string);

  coord->y = float8in_internal(str, &str, type_name, orig_string);

  *endptr = str;
}

static inline void
coord2d_sequence_decode(char *str,
                        struct coord2d *coords,
                        int ncoords,
                        char **endptr,
                        const char *type_name,
                        const char *orig_string)
{


	for(int i = 0; i < ncoords; ++i)
	{
		while (*str != '\0' && isspace((unsigned char) *str))
	    ++str;

		if(strncasecmp(str, GEOEXT_GEOPOINT_WKT_TOKEN, GEOEXT_GEOPOINT_WKT_TOKEN_LEN) != 0)
			ereport(ERROR,
	            (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
	            errmsg("invalid input syntax for type %s: \"%s\"",
	            type_name, orig_string)));

		str += GEOEXT_GEOPOINT_WKT_TOKEN_LEN;

		while (*str != '\0' && isspace((unsigned char) *str))
			++str;

	  if(*str != LDELIM)
	    ereport(ERROR,
	            (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
	            errmsg("invalid input syntax for type %s: \"%s\"",
	            type_name, orig_string)));

	  ++str;

		

	  coord2d_decode(str, coords, &str, type_name, orig_string);

		/* If we don't find a ')' after reading the x and y coordinates, the WKT is invalid */
		if (*str != RDELIM)
		  ereport(ERROR,
		          (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
		          errmsg("invalid input syntax for type")));

		/* skip the ')' */
		++str;

		while (*str != '\0' && isspace((unsigned char) *str))
			++str;



		coords->time = timestamp_decode(&str);



		/*skip ;*/
	  if(*str == COLLECTION_DELIM)
	      ++str;

	    ++coords;
	 }

  *endptr = str;

}


void spatiotemporal_decode(char *str, struct spatiotemporal *st)
{
	char *cp = str;

	Timestamp start_time = 0;

	Timestamp end_time = 0;

	while(isspace(*cp))
		cp++;

	if(strncasecmp(cp, ST_WKT_TOKEN, ST_WKT_TOKEN_LEN) == 0)
	{
		cp += ST_WKT_TOKEN_LEN;



		if(strncasecmp(cp, TRAJECTORY_WKT_TOKEN, TRAJECTORY_WKT_TOKEN_LEN) == 0)
		{
			cp += TRAJECTORY_WKT_TOKEN_LEN;

			while(isspace(*cp))
				++cp;

			if(*cp !=  LDELIM)
				ereport(ERROR,
					(errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
						errmsg("invalid input syntax for type ( not found")));

			/*skip RLELIM*/
			++cp;

			while(isspace(*cp))
				++cp;

			start_time = timestamp_decode(&cp);

			/*skip ;*/
			++cp;


			end_time = timestamp_decode(&cp);

			/*skip ;*/
			++cp;


			st->start_time = start_time;

			st->end_time = end_time;

			/* get sequence of TRAJECTORY*/



			coord2d_sequence_decode(cp, st->coords, st->npts, &cp,
                          "spatiotemporal", str);



			/* end string of TRAJECTORY*/
			while(isspace(*cp))
				cp++;

			if (*cp != RDELIM)
				ereport(ERROR,
					(errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
						errmsg("invalid input syntax for type ) not found ")));

		  /* skip the ')' */
			++cp;


		  /* skip spaces, if any */
			while (*cp != '\0' && isspace((unsigned char) *cp))
				++cp;

		  /* if we still have characters, the WKT is invalid */
			if(*cp != '\0')
				ereport(ERROR,
					(errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
						errmsg("invalid input syntax for type string fail")));

		}
		else ereport(ERROR,(errmsg("Invalide input for SPATIOTEMPORAL type")));;



	}
	else ereport(ERROR,(errmsg("Invalide input for SPATIOTEMPORAL type")));


}
